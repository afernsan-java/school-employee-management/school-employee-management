/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import java.io.Serializable;

/**
 * 
 * @author Abraham Fernandez Sanchez 
 */
public class EmpleadoClass implements Serializable{

    private String nombre;  
    private String apellidos;
    private String titulacion;
    private String direccion;
    private String nif;
    private int telefono;
    private int edad;
    private String fechaAlta;
    private boolean interino;
    /**
     * Constructor vacio
     */
    public EmpleadoClass() {
    }    
    
    /**
     * 
     * Constructor para los empleados que son profesores
     * 
     * @param nombre String 
     * @param apellidos String
     * @param titulacion String
     * @param direccion String
     * @param nif String
     * @param telefono Integer
     * @param edad Integer
     * @param fechaAlta String
     * @param interino boolean
     */
    public EmpleadoClass(String nombre, String apellidos, String titulacion, String direccion, String nif, int telefono, int edad, String fechaAlta, boolean interino) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.titulacion = titulacion;
        this.direccion = direccion;
        this.nif = nif;
        this.telefono = telefono;
        this.edad = edad;
        this.fechaAlta = fechaAlta;
        this.interino = interino;
    }

    /**
     * Constructor para los empleados que no son profesores
     * 
     * @param nombre String 
     * @param apellidos String
     * @param titulacion String
     * @param direccion String
     * @param nif String
     * @param telefono Integer
     * @param edad Integer
     * @param fechaAlta String
     */
    public EmpleadoClass(String nombre, String apellidos, String titulacion, String direccion, String nif, int telefono, int edad, String fechaAlta) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.titulacion = titulacion;
        this.direccion = direccion;
        this.nif = nif;
        this.telefono = telefono;
        this.edad = edad;
        this.fechaAlta = fechaAlta;
    }    

    /**
     * 
     * @return String nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre String nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * 
     * @return String apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * 
     * @param apellidos String Apellidos
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * 
     * @return String titulacion
     */
    public String getTitulacion() {
        return titulacion;
    }

    /**
     * 
     * @param titulacion String titulacion
     */
    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    /**
     * 
     * @return String direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * 
     * @param direccion String direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * 
     * @return String nif
     */
    public String getNif() {
        return nif;
    }

    /**
     * 
     * @param nif String nif
     */
    public void setNif(String nif) {
        this.nif = nif;
    }

    /**
     * 
     * @return Integer telefono
     */    
    public int getTelefono() {
        return telefono;
    }

    /**
     * 
     * @param telefono Integer Telefono
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * 
     * @return Integer edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * 
     * @param edad Integer edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * 
     * @return String Fecha Alta
     */
    public String getFechaAlta() {
        return fechaAlta;
    }

    /**
     * 
     * @param fechaAlta String Fecha Alta
     */
    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * 
     * @return boolean interino
     */
    public boolean isInterino() {
        return interino;
    }

    /**
     * 
     * @param interino boolean interino
     */
    public void setInterino(boolean interino) {
        this.interino = interino;
    }
    

}
