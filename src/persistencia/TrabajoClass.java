/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import java.io.Serializable;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class TrabajoClass implements Serializable{

    private String nombreTrabajo;

    /**
     * 
     * @param nombreTrabajo String nombreTrabajo
     */
    public TrabajoClass(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }

    /**
     * 
     * @return String nombre Trabajo
     */
    public String getNombreTrabajo() {
        return nombreTrabajo;
    }

    /**
     * 
     * @param nombreTrabajo String nombre Trabajo
     */
    public void setNombreTrabajo(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }
    
    
}
