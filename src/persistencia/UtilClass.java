/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class UtilClass implements Serializable{
    
    static final String ARCHIVO_EMPLEADOS = "empleados.dat";
    static final String ARCHIVO_TRABAJOS = "trabajos.dat";
    
    static private ArrayList<EmpleadoClass> listaEmpleados = new ArrayList<>();     
    static private ArrayList<TrabajoClass> listaTrabajos = new ArrayList<>();
    
    EmpleadoClass empleado;
    TrabajoClass trabajo;    
    
    /**
     * Añadira un nuevo empleado al arrayList: listaEmpleados
     * @param empleado Este es un objeto de la clase EmpleadoClass
     */
    public void addNewEmpleado(EmpleadoClass empleado){    
        listaEmpleados.add(empleado);
    }  
    
    /**
     * Metodo que devuelve al dimension del arrayList listaEmpleados <br>
     * @return Integer con la dimension del array
     */
    public int sizeListaEmpleados(){
        return listaEmpleados.size();
    }
    
    /**
     * Metodo que devuelve un objeto empleado correspondiente a la posicion del arrayList introducida <br>
     * @param index Integer correspondeinete a la posicion
     * @return Devuleve un objeto empleado de la clase EmpleadoClass
     */
    public EmpleadoClass getEmpleado(int index){        
        return empleado = listaEmpleados.get(index); 
    }    
    
    /**
     * Metodo para modificar los datos de un empleado <br>
     * @param empleado Objeto de la clase EmpleadoClass
     */
    public void modifiEmpleado(EmpleadoClass empleado){  
        String nif = empleado.getNif();
        deleteEmpleado(nif);
        addNewEmpleado(empleado);    
    }
    
    /**
     * Metodo para Eliminar un Empleado del Array listaEmpleados <br>
     * @param nif String del nif del empleado
     */
    public void deleteEmpleado(String nif){
        for (int i = 0; i < listaEmpleados.size(); i++) {            
            empleado = listaEmpleados.get(i);              
            if(empleado.getNif().equalsIgnoreCase(nif)){
                listaEmpleados.remove(i);
            }
        }  
    }
    
    /**
     * 
     * Metodo que guardara el arrayList listaEmpleados como un objeto en un archivo externo <br>
     * 
     * @throws FileNotFoundException Excepcion
     * @throws IOException Excepcion
     */
    public void saveFileEmpleados() throws FileNotFoundException, IOException{        
        File file = new File(ARCHIVO_EMPLEADOS); 
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(listaEmpleados);
        oos.close();    
    }
    
    /**
     * 
     * Metodo que inserta la informacion de un archivo externo en el array listaEmpleados <br>
     * 
     * @throws FileNotFoundException Excepcion
     * @throws IOException Excepcion
     * @throws ClassNotFoundException  Excepcion
     */
    public void loadFileEmpleados() throws FileNotFoundException, IOException, ClassNotFoundException{        
        File file = new File(ARCHIVO_EMPLEADOS);        
        if(file.exists()){        
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            listaEmpleados = (ArrayList<EmpleadoClass>) ois.readObject(); 
        }
    }
    
    /**
     * 
     * Metodo para encontrar un empleado por su nif <br>
     * 
     * @param nif String del documento nif del empleado
     * @return retorna un objeto empleado de la clase EmpleadoClass
     */
    public EmpleadoClass findEmpleado(String nif){        
        for (int i = 0; i < listaEmpleados.size(); i++) {            
            empleado = listaEmpleados.get(i);              
            if(empleado.getNif().equalsIgnoreCase(nif)){
                return empleado;
            }
        }
        return null;      
    }
    
    /**
     * Metodo para mostrar la informacion del arrayList listaEmpleados por consola
     */
    public void showArrayListListaEmpleados(){
        empleado = new EmpleadoClass();
        
        for (int i = 0; i < listaEmpleados.size(); i++) {
            empleado = listaEmpleados.get(i); 
            System.out.println("---------------------------");        
            System.out.println(empleado.getNombre());
        }          
    }
    
    //-------------------------------------------------------------    
    
    /**
     * Añadira un nuevo empleado al arrayList: listaTrabajos <br>
     * @param trabajo  Este es un objeto de la clase TrabajoClass
     */
    public void addnewTrabajo(TrabajoClass trabajo){
        listaTrabajos.add(trabajo);
    }
    
    /**
     * Metodo que devuelve al dimension del arrayList listaTrabajos <br>
     * @return Integer de la dimension del array listaTrabajos
     */
    public int sizeListaTrabajos(){
        return listaTrabajos.size();
    }
    
    /**
     * Metodo que devuelve el nombre del trabajo correspondiente a la posicion del arrayList introducida <br>
     * @param index Integer corresponidente a la posicion dentro del arrayList
     * @return String del nombre del trabajo correspondiente
     */
    public String getTrabajo(int index){        
        trabajo = listaTrabajos.get(index);        
        return trabajo.getNombreTrabajo();
    }  
    
    /**
     * Metodo para modificar los datos de un trabajo <br>
     * @param trabajo Objeto de la clase TrabajoClass
     */
    public void modifiTrabajo(TrabajoClass trabajo){
        String nombreTrabajo = trabajo.getNombreTrabajo();
        addnewTrabajo(trabajo);
    }
    
    /**
     * Metodo para Eliminar un Trabajo del Array listaTrabajos <br>
     * @param nombreTrabajo String nombre del trabajo a eliminar
     */
    public void deleteTrabajo(String nombreTrabajo){
        for (int i = 0; i < listaTrabajos.size(); i++) {            
            trabajo = listaTrabajos.get(i);              
            if(trabajo.getNombreTrabajo().equalsIgnoreCase(nombreTrabajo)){
                listaTrabajos.remove(i);
            }
        }  
    }        
    
    /**
     * 
     * Metodo que guardara el arrayList listaTrabajos como un objeto en un archivo externo <br>
     * 
     * @throws FileNotFoundException Excepcion
     * @throws IOException Excepcion
     */
    public void saveFileTrabajos() throws FileNotFoundException, IOException{        
        File file = new File(ARCHIVO_TRABAJOS); 
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(listaTrabajos);
        oos.close();    
    }
    
    /**
     * 
     * Metodo que inserta la informacion de un archivo externo en el array listaTrabajos <br>
     * 
     * @throws FileNotFoundException Excepcion
     * @throws IOException Excepcion
     * @throws ClassNotFoundException Excepcion
     */
    public void loadFileTrabajos() throws FileNotFoundException, IOException, ClassNotFoundException{        
        File file = new File(ARCHIVO_TRABAJOS);        
        if(file.exists()){        
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            listaTrabajos = (ArrayList<TrabajoClass>) ois.readObject(); 
        }
    }
        
    
    
}
